import Vue from 'vue'
	import Router from 'vue-router'
	import HelloWorld from './components/HelloWorld.vue'
    import Contact from './components/Contact.vue'
    import ContactList from './components/ContactList.vue'
	Vue.use(Router)
	export default new Router({
	  routes: [
		{
		  path: '/',
		  name: 'helloworld',
		  component: HelloWorld
        },
        {
            path: '/contactlist',
            name: 'contactlist',
            component: ContactList
          },
		{
		  path: '/contact',
		  name: 'contact',
		  component: Contact
		}
	  ]
    })
    


